import csv
import os
import logging
from itertools import chain 

def lecture_csv(nomFichier,delimiteur):
    fileContent = ""
    with open(nomFichier) as csvfile:
        reader = csv.reader(csvfile, delimiter=delimiteur)
        fileContent = [row for row in reader]
    return fileContent


