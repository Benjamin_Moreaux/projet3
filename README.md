# ![](imgs/logo.png)
# M3301 - Mini projet 2

## Description

Adaptation d'un CSV pour une autre BDD.

## Lancer le projet

```bash
$ python3 console.py auto.csv
```
## Tester le projet

```bash
$ python3 -m unittest
```

## Diagramme de cas d'utilisation

![diagramme_activite.png](imgs/uml/diagramme_activite.png)

## Diagramme d'activité

![diagrammeactivitepng](imgs/uml/diagramme_cas_utilisations.png)

## Analyse Fonctionnelle

![diagrammeactivitepng](imgs/uml/analyse.png)

## Auteurs

- [BERGÈS Dimitri](https://gitlab.com/Dixmis)
- [GENNEVEE Hugo](https://gitlab.com/HugoGennevee)