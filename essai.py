import sqlite3
import logging
import os.path
import csv
from datetime import *

def verif(nombdd):
    if os.path.isfile(nombdd):
        connection = sqlite3.connect(nombdd)
        logging.info("Connection à la base donnée")
        return connection
    else:
        open(nombdd,'w')
        logging.debug("Création de la base donnée")

def verif_bdd(connection):
    cursor = connection.cursor()
    cursor.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='siv' ''')
    if cursor.fetchone()[0]==1 :
        logging.debug("La table siv exite déjà")
    else:
        cursor.execute('''.read table.sql''')
        logging.debug("Création de la table siv")

def lecture_csv(nomFichier,delimiteur):
    fileContent = ""
    with open(nomFichier) as csvfile:
        reader = csv.reader(csvfile, delimiter=delimiteur)
        fileContent = [row for row in reader]
    return fileContent

def bdd(Content, connection):
    cursor = connection.cursor()
    for row in range(22):
        if(Content == cursor.execute("SELECT * FROM siv")):
            logging.warning("Donnee déjà insérer ! ")
        else:
            cursor.execute("INSERT INTO siv VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", Content)
    logging.info("Données insérer")
    connection.commit()
    

logging.basicConfig(filename='{datetime.datetime.now()}.log',level=logging.INFO, format="%(asctime)s %(levelname)s %(message)s")
logging.info("Commencement : ")

print("Entrer nom bdd : (.sqlite3) déjà ajouté :")
nombdd = input() + ".sqlite3"
connection = verif(nombdd)

print(connection)
verif_bdd(connection)
Content = lecture_csv('auto.csv','|')

bdd(Content, connection)

connection.close()
logging.info("Fin du processus")